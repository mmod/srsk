#version 330 core
out vec4 FragColor;

uniform vec3 lampColor;

void main()
{
    FragColor = vec4( lampColor.x, lampColor.y, lampColor.z, 1.0); // set the lamp color
}