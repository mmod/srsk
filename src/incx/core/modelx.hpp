/**
 * package: srsk
 * sub-package: src/incx/core/modelx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef MODELX_HPP
#define MODELX_HPP


// INCLUDES
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "virtualx.hpp"
#include "meshx.hpp"
#include "shaderx.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>


class modelx
{
    public:
        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;
        glm::vec3 right;
        glm::vec3 worldUp;

        float yaw;
        float pitch;

        float movementSpeed;
        float mouseSensitivity;
        float zoom;

        std::vector<texturex> textures_loaded;
        std::vector<meshx> meshes;
        std::string directory;
        bool gammaCorrection;



        modelx( std::string const &path, bool gamma = false, glm::vec3 positionx = glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3 upx = glm::vec3( 0.0f, 1.0f, 0.0f ), float yawx = YAW, float pitchx = PITCH );
        //modelx( std::string const &path, bool gamma = false, float x, float y, float z, float X, float Y, float Z, float yawx, float pitchx );


        void draw( shaderx* shader );
        void processKeyboard( SPATIAL_MOVEMENT direction, float deltaTime );

    private:


        /**
         * loadModel
         * 
         * loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
         */
        void loadModel( std::string const &path );


        /**
         * processNode
         * 
         * processes a node in a recursive fashion. Processes each individual mesh located at the node and 
         * repeats this process on its children nodes (if any).
         */
        void processNode( aiNode* node, const aiScene* scene );


        meshx processMesh( aiMesh* mesh, const aiScene* scene );


        /**
         * loadMaterialTextures
         * 
         * checks all material textures of a given type and loads the textures if they're not loaded yet.
         * the required info is returned as a Texture struct.
         */
        std::vector<texturex> loadMaterialTextures( aiMaterial* mat, aiTextureType type, std::string typeName );


        void updateObjectVectors();

};


#endif