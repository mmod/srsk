/**
 * package: srsk
 * sub-package: src/incx/core/shaderx.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


 #include "shaderx.hpp"


 shaderx::shaderx( const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr )
 {
    // Ensure ifstream objects can throw exceptions
    std::ifstream v;
    std::ifstream f;
    std::ifstream g;

    v.exceptions( std::ifstream::failbit | std::ifstream::badbit );
    f.exceptions( std::ifstream::failbit | std::ifstream::badbit );
    g.exceptions( std::ifstream::failbit | std::ifstream::badbit );

    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;

    // Retrieve the vertex source code from filepath
    try
    {
        v.open( vertexPath );
        vertexCode = std::string( ( std::istreambuf_iterator<char>( v ) ), std::istreambuf_iterator<char>() );
        //vertexCode = vertexCodeX;
        //std::cout << "Vertex Code: " << std::endl;
        //std::cout << vertexCode.c_str() << std::endl << std::endl;

        // Retrieve the fragment source code from filepath
        f.open( fragmentPath );
        fragmentCode = std::string( ( std::istreambuf_iterator<char>( f ) ), std::istreambuf_iterator<char>() );
        //fragmentCode = fragmentCodeX;
        //std::cout << "Fragment Code: " << std::endl;
        //std::cout << fragmentCode.c_str() << std::endl << std::endl;

        if( geometryPath != nullptr )
        {
            // Retrieve the geometry source code from filepath
            g.open( geometryPath );
            geometryCode = std::string( ( std::istreambuf_iterator<char>( g ) ), std::istreambuf_iterator<char>() );
            //geometryCode = geometryCodeX;
            //std::cout << "Geometry Code: " << std::endl;
            //std::cout << geometryCode.c_str() << std::endl << std::endl;
        }
    }
    catch( std::ifstream::failure e )
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ" << std::endl;
    }
    
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    // Compile the shaders
    unsigned int vertex, fragment, geometry;
    int success; 
    char infoLog[512];

    // vertex
    vertex = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( vertex, 1, &vShaderCode, NULL );
    glCompileShader( vertex );
    this->checkCompileErrors( vertex, "VERTEX" );

    // fragment
    fragment = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( fragment, 1, &fShaderCode, NULL );
    glCompileShader( fragment );
    this->checkCompileErrors( fragment, "FRAGMENT" );

    // geometry
    if( geometryPath != nullptr )
    {
        const char* gShaderCode = geometryCode.c_str();
        geometry = glCreateShader( GL_GEOMETRY_SHADER );
        glShaderSource( geometry, 1, &gShaderCode, NULL );
        glCompileShader( geometry );
        this->checkCompileErrors( geometry, "GEOMETRY" );
    }

    // Shader PROGRAM
    this->id = glCreateProgram();
    glAttachShader( this->id, vertex );
    glAttachShader( this->id, fragment );
    if( geometryPath != nullptr )
    {
        glAttachShader( this->id, geometry );
    }
    glLinkProgram( this->id );
    checkCompileErrors( this->id, "PROGRAM" );

    // Delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader( vertex );
    glDeleteShader( fragment );
    if( geometryPath != nullptr )
    {
        glDeleteShader( geometry );
    }
    
 }


 shaderx::~shaderx()
 {
 }


void shaderx::use()
{
    glUseProgram( this->id );
}


void shaderx::setBool( const std::string &name, bool value )
{
    glUniform1i( glGetUniformLocation( this->id, name.c_str() ), ( int )value );
}


void shaderx::setInt( const std::string &name, int value )
{
    glUniform1i( glGetUniformLocation( this->id, name.c_str() ), value );
}


void shaderx::setFloat( const std::string &name, float value )
{
    glUniform1f( glGetUniformLocation( this->id, name.c_str() ), value ); 
}


void shaderx::setVec2( const std::string &name, const glm::vec2 &value )
{
    glUniform2fv( glGetUniformLocation( this->id, name.c_str() ), 1, &value[0] );
}


void shaderx::setVec2( const std::string &name, float x, float y )
{
    glUniform2f( glGetUniformLocation( this->id, name.c_str() ), x, y );
}


void shaderx::setVec3( const std::string &name, const glm::vec3 &value )
{
    glUniform3fv( glGetUniformLocation( this->id, name.c_str() ), 1, &value[0] );
}


void shaderx::setVec3( const std::string &name, float x, float y, float z )
{
    glUniform3f( glGetUniformLocation( this->id, name.c_str() ), x, y, z ); 
}


void shaderx::setVec4( const std::string &name, const glm::vec4 &value )
{
    glUniform4fv( glGetUniformLocation( this->id, name.c_str() ), 1, &value[0] );
}


void shaderx::setVec4( const std::string &name, float x, float y, float z, float w )
{
    glUniform4f( glGetUniformLocation( this->id, name.c_str() ), x, y, z, w );
}


void shaderx::setMat2( const std::string &name, const glm::mat2 &mat )
{
    glUniformMatrix2fv( glGetUniformLocation( this->id, name.c_str() ), 1, GL_FALSE, &mat[0][0] );
}


void shaderx::setMat3( const std::string &name, const glm::mat3 &mat )
{
    glUniformMatrix3fv( glGetUniformLocation( this->id, name.c_str() ), 1, GL_FALSE, &mat[0][0] );
}


void shaderx::setMat4( const std::string &name, const glm::mat4 &mat )
{
    glUniformMatrix4fv( glGetUniformLocation( this->id, name.c_str() ), 1, GL_FALSE, &mat[0][0] );
}


void shaderx::checkCompileErrors( GLuint shader, std::string type )
{
    GLint success;
    GLchar infoLog[1024];

    if( type != "PROGRAM" )
    {
        glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
        if( !success )
        {
            glGetShaderInfoLog( shader, 1024, NULL, infoLog );
            std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
    else
    {
        glGetProgramiv( shader, GL_LINK_STATUS, &success );
        if( !success )
        {
            glGetProgramInfoLog( shader, 1024, NULL, infoLog );
            std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
        }
    }
}