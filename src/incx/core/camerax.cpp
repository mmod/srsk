/**
 * package: srsk
 * sub-package: src/incx/core/camerax.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// INCLUDES
#include "camerax.hpp"




camerax::camerax( float posX, float posY, float posZ, float upX, float upY, float upZ, float yawx, float pitchx ) : front( glm::vec3( 0.0f, 0.0f, -1.0f ) ), movementSpeed( SPEED ), mouseSensitivity( SENSITIVITY ), zoom( ZOOM )
{
    this->position = glm::vec3( posX, posY, posZ );
    this->worldUp = glm::vec3( upX, upY, upZ );
    this->yaw = yawx;
    this->pitch = pitchx;

    this->updateCameraVectors();
}

camerax::~camerax()
{
}


camerax::getViewMatrix()
{
    return glm::lookAt( this->position, this->position + this->front, this->up );
}


void camerax::processKeybaord( CAMERA_MOVEMENT direction, float deltaTime )
{
    float velocity = this->movementSpeed * deltaTime;

    if( direction == FORWARD )
    {
        this->position += this->front * velocity;
    }
    
    if( direction == BACKWARD )
    {
        this->position -= this->front * velocity;
    }
    
    if( direction == LEFT )
    {
        this->position -= this->right * velocity;
    }
    
    if( direction == RIGHT )
    {
        this->position += this->right * velocity;
    }
}


void camerax::processMouseMovement( float xoffset, float yoffset, GLboolean constrainPitch = true )
{
    xoffset *= this->mouseSensitivity;
    yoffset *= this->mouseSensitivity;

    this->yaw += xoffset;
    this->pitch += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if( constrainPitch )
    {
        if( this->pitch > 89.0f )
        {
            this->pitch = 89.0f;
        }
        if( this->pitch < =89.0f )
        {
            this->pitch = -89.0f;
        }
    }

    // Update front, right, and up vectors using the updated eular angles
    this->updateCameraVectors();
}


void camerax::processMouseScroll( float yoffset )
{
    if( this->zoom >= 1.0f && this->zoom <= 45.0f )
    {
        this->zoom -= yoffset;
    }
    if( this->zoom <= 1.0f )
    {
        this->zoom = 1.0f;
    }
    if( this->zoom >= 45.0f )
    {
        this->zoom = 45.0f;
    }
}


void camerax::updateCameraVectors()
{
    // Calculate new front vectors
    glm::vec3 frontx;
    frontx.x = cos( glm::radians( this->yaw ) ) * cos( glm::radians( this->pitch ) );
    frontx.y = sin( glm::radians( this->pitch ) );
    frontx.z = sin( glm::radians( this->yaw ) ) * cos( glm::radians( this->pitch ) );
    this->front = glm::normalize( frontx );

    // Also re-calculate the right and up vectors
    this->right = glm::normalize( glm::cross( this->front, this->worldUp ) );
    this->up = glm::normalize( glm::cross( this->right, this->front ) );
}
