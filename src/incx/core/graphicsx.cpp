/**
 * package: srsk
 * sub-package: src/incx/core/graphicsx.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// INCLUDES
#include "graphicsx.hpp"


// FORWARD DECLARATIONS
void clearLog();
void addLog( const char* fmt, ... ) __attribute__ ( ( format ( printf, 1, 2 ) ) ) ;
static int textEditCallback( const char* data );
static int textEditCallbackStub( ImGuiTextEditCallbackData* data );
static int textEditHistoryCallback( ImGuiTextEditCallbackData* data );

// Portable helpers
static int   stricmpx( const char* str1, const char* str2 );
static int   strnicmpx( const char* str1, const char* str2, int n );
static char* strdupx( const char *str );


// GLOBALS
ImVector<char*> items;
ImVector<char*> history;
int historyPos;
bool ui_system_log_scroll_to_bottom = false;

float vertices[] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

float lampVertices[] = {
    -0.5f, -0.5f, -0.5f, 
     0.5f, -0.5f, -0.5f,  
     0.5f,  0.5f, -0.5f,  
     0.5f,  0.5f, -0.5f,  
    -0.5f,  0.5f, -0.5f, 
    -0.5f, -0.5f, -0.5f, 

    -0.5f, -0.5f,  0.5f, 
     0.5f, -0.5f,  0.5f,  
     0.5f,  0.5f,  0.5f,  
     0.5f,  0.5f,  0.5f,  
    -0.5f,  0.5f,  0.5f, 
    -0.5f, -0.5f,  0.5f, 

    -0.5f,  0.5f,  0.5f, 
    -0.5f,  0.5f, -0.5f, 
    -0.5f, -0.5f, -0.5f, 
    -0.5f, -0.5f, -0.5f, 
    -0.5f, -0.5f,  0.5f, 
    -0.5f,  0.5f,  0.5f, 

     0.5f,  0.5f,  0.5f,  
     0.5f,  0.5f, -0.5f,  
     0.5f, -0.5f, -0.5f,  
     0.5f, -0.5f, -0.5f,  
     0.5f, -0.5f,  0.5f,  
     0.5f,  0.5f,  0.5f,  

    -0.5f, -0.5f, -0.5f, 
     0.5f, -0.5f, -0.5f,  
     0.5f, -0.5f,  0.5f,  
     0.5f, -0.5f,  0.5f,  
    -0.5f, -0.5f,  0.5f, 
    -0.5f, -0.5f, -0.5f, 

    -0.5f,  0.5f, -0.5f, 
     0.5f,  0.5f, -0.5f,  
     0.5f,  0.5f,  0.5f,  
     0.5f,  0.5f,  0.5f,  
    -0.5f,  0.5f,  0.5f, 
    -0.5f,  0.5f, -0.5f, 
};

float vertices3[] = {
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
};

glm::vec3 cubePositions[] = {
    glm::vec3( 0.0f,  0.0f,  0.0f),
    glm::vec3( 2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f,  2.0f, -2.5f),
    glm::vec3( 1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f)
};


graphicsx::graphicsx()
{
    this->shader = nullptr;
    this->lampShader = nullptr;
    this->playerModel = nullptr;
    this->data = nullptr;
    this->ui_show_system_log_window = true;
    //this->ui_system_log_scroll_to_bottom = false;
    this->ui_show_system_debug_window = false;
    this->ui_show_system_metrics_window = false;
    this->ui_show_system_configuration_window = false;
    this->ui_show_style_editor_window = false;
    this->ui_show_system_console_window = false;
    this->ui_show_demo_window = false;
    this->dev_tool_color_picker_value = ImVec4( 0.45f, 0.55f, 0.60f, 1.00f );
}


graphicsx::~graphicsx()
{
}


int graphicsx::init( GLFWwindow* window, int screen_width, int screen_height )
{
    this->screenWidth = screen_width;
    this->screenHeight = screen_height;

    // Set the light pos
    this->lightPos = glm::vec3( 1.2f, 1.0f, 2.0f );

    // Instantiate our shader object
    this->shader = new shaderx( "incx/shaders/vertex.glsl", "incx/shaders/fragment.glsl", nullptr );
    if( !this->shader )
    {
        std::cout << "Failed to instantiate the shader object" << std::endl;
    }

    this->lampShader = new shaderx( "incx/shaders/lampVertex.glsl", "incx/shaders/lampFragment.glsl", nullptr );
    if( !this->lampShader )
    {
        std::cout << "Failed to instantiate the lamp shader object" << std::endl;
    }


    // Generate the lamp vertex buffer and vertex array objects
    glGenVertexArrays( 1, &this->lvao );
    glGenBuffers( 1, &this->vbo );

    glBindBuffer( GL_ARRAY_BUFFER, this->vbo );
    glBufferData( GL_ARRAY_BUFFER, sizeof( vertices3 ), vertices3, GL_STATIC_DRAW );

    glBindVertexArray( this->lvao );

    // position attribute
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof( float ), ( void* )0 );
    glEnableVertexAttribArray( 0 );
    // normal attribute
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof( float ), ( void* )( 3 * sizeof( float ) ) );
    glEnableVertexAttribArray( 1 );


    // Load models
    std::cout << "Instantiating player model..." << std::endl;
    this->playerModel = new modelx( "incx/res/models/nanosuit/nanosuit.obj" );

    return 1;
}


int graphicsx::render( camerax* camera )
{
    // Window color
    glClearColor( 0.1f, 0.1f, 0.1f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Activate shader
    this->shader->use();

    this->shader->setVec3( "lightColor", 1.0f, 1.0f, 1.0f ); // A White light
    //this->shader->setVec3( "lightColor", 1.0f, 0.5f, 0.5f ); // A Red light
    this->shader->setVec3( "viewPos", camera->position );
    this->shader->setVec3( "material.ambient", 1.0f, 1.0f, 1.0f );
    this->shader->setVec3( "material.diffuse", 0.8f, 0.8f, 0.8f );
    this->shader->setVec3( "material.specular", 1.0f, 1.0f, 1.0f );
    this->shader->setFloat( "material.shine", 32.0f );
    this->shader->setVec3( "light.position", this->lightPos );
    this->shader->setVec3( "light.ambient", 0.4f, 0.4f, 0.4f );
    this->shader->setVec3( "light.diffuse", 0.7f, 0.7f, 0.7f );
    this->shader->setVec3( "light.specular", 1.2f, 1.2f, 1.2f );

    // Pass projection matrix to shader (note that in this case it could change every frame)
    this->projection = glm::mat4( 1.0f );
    this->projection = glm::perspective( glm::radians( camera->zoom ), ( float )this->screenWidth / ( float )this->screenHeight, 0.1f, 100.0f );
    this->shader->setMat4( "projection", this->projection );

    // Camera/view transformation
    this->view = glm::mat4( 1.0f );
    this->view = camera->getViewMatrix();
    this->shader->setMat4( "view", this->view );

    // Render the loaded model
    this->model = glm::mat4( 1.0f );
    //this->model = glm::translate( this->model, this->playerModel->position );
    this->model = glm::translate( this->model, glm::vec3( 0.0f, -1.75f, 0.0f ) );   // Translate it down so its at the center of the scene
    this->model = glm::scale( this->model, glm::vec3( 0.2f, 0.2f, 0.2f ) );         // Its a bit too big for our scene, so scale it down
    this->shader->setMat4( "model", this->model );
    this->playerModel->draw( this->shader );

    // Render the lamp object
    this->lampShader->use();
    this->lampShader->setVec3( "lampColor", 1.0f, 1.0f, 1.0f ); // A White lamp
    //this->lampShader->setVec3( "lampColor", 5.0f, 0.0f, 0.0f ); // A Red lamp

    this->lampShader->setMat4( "projection", this->projection );
    this->lampShader->setMat4( "view",this->view );
    this->model = glm::mat4( 1.0f );
    this->model = glm::translate( this->model, this->lightPos );
    this->model = glm::scale( this->model, glm::vec3( 0.2f ) ); // a smaller cube
    this->lampShader->setMat4( "model", this->model );

    glBindVertexArray( this->lvao );
    glDrawArrays( GL_TRIANGLES, 0, 36 );


    // Handle user interfaace
    int renderedUI = this->renderUI();
    

    return 0;

}


int graphicsx::renderUI()
{
    // Call NewFrame first
    ImGui_ImplGlfwGL3_NewFrame();

    // Process UI components as required
    if( this->ui_show_system_log_window )
    {
        //ImGui::SetNextWindowPos( ImVec2( 650, 20 ), ImGuiCond_FirstUseEver ); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        this->showSystemLogWindow( &this->ui_show_system_log_window );
    }
    if( this->ui_show_system_debug_window )
    {
        //ImGui::SetNextWindowPos( ImVec2( 650, 20 ), ImGuiCond_FirstUseEver ); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        this->showSystemDebugWindow( &this->ui_show_system_debug_window );
    }
    if( this->ui_show_system_configuration_window )
    {
        //ImGui::SetNextWindowPos( ImVec2( 650, 20 ), ImGuiCond_FirstUseEver ); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        this->showSystemConfigurationWindow( &this->ui_show_system_configuration_window );
    }
    if( this->ui_show_system_metrics_window )
    {
        ImGui::ShowMetricsWindow( &this->ui_show_system_metrics_window );
    }
    if( this->ui_show_style_editor_window )
    {
        ImGui::Begin( "Style Editor", &this->ui_show_style_editor_window ); 
        ImGui::ShowStyleEditor(); 
        ImGui::End();
    }
    if( this->ui_show_demo_window )
    {
        //ImGui::SetNextWindowPos( ImVec2( 650, 20 ), ImGuiCond_FirstUseEver ); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        ImGui::ShowDemoWindow( &this->ui_show_demo_window );
    }

    ImGui::Render();
}


void graphicsx::showSystemDebugWindow( bool* p_open )
{

    // Show the debug Window
    // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
    {
        static float f = 0.0f;
        ImGui::Text( "SRSK Frame Information" );                            // Some text (you can use a format string too)
        ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );

        if( ImGui::Button( "Show Metrics" ) )
        {
            this->ui_show_system_metrics_window ^= 1;
        }
        
        ImGui::Separator();

        ImGui::Text( "SRSK Developer/Debug Tools" );
        ImGui::ColorEdit3( "Color Picker", ( float* )&this->dev_tool_color_picker_value );    // Edit 3 floats as a color

        if( ImGui::Button( "Style Editor" ) )
        {
            this->ui_show_style_editor_window ^= 1;
        }
        if( ImGui::Button( "System Configuration" ) )
        {
            this->ui_show_system_configuration_window ^= 1;
        }
        if( ImGui::Button( "IMGUI Demo" ) )
        {
            this->ui_show_demo_window ^= 1;
        }
    }
}


void graphicsx::showSystemConfigurationWindow( bool* p_open )
{
    static bool no_titlebar = false;
    static bool no_scrollbar = false;
    static bool no_menu = false;
    static bool no_move = false;
    static bool no_resize = false;
    static bool no_collapse = false;
    static bool no_close = false;

    // Demonstrate the various window flags. Typically you would just use the default.
    ImGuiWindowFlags window_flags = 0;
    if( no_titlebar )  window_flags |= ImGuiWindowFlags_NoTitleBar;
    if( no_scrollbar ) window_flags |= ImGuiWindowFlags_NoScrollbar;
    if( !no_menu )     window_flags |= ImGuiWindowFlags_MenuBar;
    if( no_move )      window_flags |= ImGuiWindowFlags_NoMove;
    if( no_resize )    window_flags |= ImGuiWindowFlags_NoResize;
    if( no_collapse )  window_flags |= ImGuiWindowFlags_NoCollapse;
    if( no_close )     p_open = NULL; // Don't pass our bool* to Begin
    ImGui::SetNextWindowSize( ImVec2( 550,680 ), ImGuiCond_FirstUseEver );
    if( !ImGui::Begin( "System Configuration", p_open, window_flags ) )
    {
        // Early out if the window is collapsed, as an optimization.
        ImGui::End();
        return;
    }

    //ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.65f);    // 2/3 of the space for widget and 1/3 for labels
    ImGui::PushItemWidth( -140 );                                 // Right align, keep 140 pixels for labels

    ImGui::Text("SRSK says hello. (%s)", IMGUI_VERSION );





    ImGui::End();
}


void graphicsx::showSystemLogWindow( bool* p_open )
{
    static bool no_titlebar = true;
    static bool no_scrollbar = false;
    static bool no_menu = true;
    static bool no_move = false;
    static bool no_resize = false;
    static bool no_collapse = true;
    static bool no_close = true;

    // Demonstrate the various window flags. Typically you would just use the default.
    ImGuiWindowFlags window_flags = 0;
    if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
    if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
    if (!no_menu)     window_flags |= ImGuiWindowFlags_MenuBar;
    if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
    if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
    if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;
    if (no_close)     p_open = NULL; // Don't pass our bool* to Begin

    ImGui::SetNextWindowSize( ImVec2( 512,256 ), ImGuiCond_FirstUseEver );
    ImGui::SetNextWindowPos( ImVec2( 24, 684 ), ImGuiCond_FirstUseEver );
    
    // Start a window
    if( !ImGui::Begin( "System Log", p_open, window_flags ) )
    {
        // Early out if the window is collapsed, as an optimization.
        ImGui::End();
        return;
    }
    
    static int line = 50;
    //static ImGuiTextFilter filter;
    //filter.Draw( "Filter (\"incl,-excl\") (\"error\")", 180 );

    // A child area for containing the lines of messages
    ImGui::BeginChild( "ChatLog", ImVec2( ImGui::GetWindowContentRegionWidth() /* * 0.5f */, 300 ), false, 0 );
    
    if( ImGui::BeginPopupContextWindow() )
    {
        if( ImGui::Selectable( "Clear" ) ) 
        {
            clearLog();
        }
        ImGui::EndPopup();
    }

    // Chat messages would go here
    ImGui::Text( "Welcome to The Sovereign Republic of Sihl Krel! (%s)", SRSK_VERSION );
    ImVec4 col_default_text = ImGui::GetStyleColorVec4( ImGuiCol_Text );
    for( int i = 0; i < items.Size; i++ )
    {
        const char* item = items[i];
        //if( !filter.PassFilter( item ) )
        //{
        //    continue;
        //}
        ImVec4 col = col_default_text;
        if( strstr( item, "[error]" ) )
        {
            col = ImColor( 1.0f,0.4f,0.4f,1.0f );
        }
        else if( strncmp( item, "# ", 2 ) == 0 )
        { 
            col = ImColor( 1.0f,0.78f,0.58f,1.0f );
        }
        ImGui::PushStyleColor( ImGuiCol_Text, col );
        ImGui::TextUnformatted( item );
        //ImGui::Text( item );
        ImGui::PopStyleColor();
    }

    // Scroll to bottom
    if( ui_system_log_scroll_to_bottom )
    {
        ImGui::SetScrollHere();
        ui_system_log_scroll_to_bottom = false;
    }
            
    ImGui::EndChild();

    // A text input field for entering messages
    char buf1[256] = ""; 
    ImGui::PushItemWidth( -1 );
    if( ImGui::InputText( "Input", buf1, IM_ARRAYSIZE( buf1 ), ImGuiInputTextFlags_EnterReturnsTrue|ImGuiInputTextFlags_CallbackHistory, &textEditCallbackStub, ( void* )this ) )
    {
        char* input_end = buf1 + strlen( buf1 );
        while( input_end > buf1 && input_end[-1] == ' ' )
        { 
            input_end--; 
        } 
        
        *input_end = 0;
        
        if( buf1[0] )
        {
            textEditCallback( buf1 );
        }
        strcpy( buf1, "" );
    }

    // Demonstrate keeping auto focus on the input box
    if( ImGui::IsItemHovered() || ( ImGui::IsWindowFocused( ImGuiFocusedFlags_RootAndChildWindows ) && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked( 0 ) ) )
    {
        ImGui::SetKeyboardFocusHere( -1 ); // Auto focus previous widget
    }

    //ImGui::InputText( "default", buf1, 64 );
    ImGui::End();
}


void graphicsx::shutdown()
{
    // de-allocate all resources once they've outlived their purpose
    glDeleteVertexArrays( 1, &this->vao );
    glDeleteVertexArrays( 1, &this->lvao );
    glDeleteBuffers( 1, &this->vbo );
     
     // Destroy the player object
     if( this->playerModel )
     {
         delete this->playerModel;
         this->playerModel = nullptr;
     }

    // Destroy our shader object
    if( this->shader )
    {
        delete this->shader;
        this->shader = nullptr;
    }

    if( this->lampShader )
    {
        delete this->lampShader;
        this->lampShader = nullptr;
    }

    // Shutdown the User Interface
    ImGui_ImplGlfwGL3_Shutdown();
}


void clearLog()
{
    for( int i = 0; i < items.Size; i++ )
    {
        free( items[i] );
    }
    items.clear();
    ui_system_log_scroll_to_bottom = true;
}

void addLog( const char* fmt, ... )
{
    // FIXME-OPT
    char buf[1024];
    va_list args;
    va_start( args, fmt );
    vsnprintf( buf, IM_ARRAYSIZE( buf ), fmt, args );
    buf[IM_ARRAYSIZE( buf )-1] = 0;
    va_end( args );
    items.push_back( strdupx( buf ) );
    ui_system_log_scroll_to_bottom = true;
}


static int textEditCallback( const char* data ) // In C++11 you are better off using lambdas for this sort of forwarding callbacks
{
    //std::cout << "The string: " << data << std::endl;
    //std::string item = data;
    addLog("# %s\n", data );
    //items.push_back( item );
    return 0;
}


static int textEditCallbackStub( ImGuiTextEditCallbackData* data ) // In C++11 you are better off using lambdas for this sort of forwarding callbacks
{
    return textEditHistoryCallback( data );
}


static int textEditHistoryCallback( ImGuiTextEditCallbackData* data )
{
    //AddLog("cursor: %d, selection: %d-%d", data->CursorPos, data->SelectionStart, data->SelectionEnd);
    switch( data->EventFlag )
    {
        case ImGuiInputTextFlags_CallbackHistory:
        {
            // Example of HISTORY
            const int prev_history_pos = historyPos;
            if( data->EventKey == ImGuiKey_UpArrow )
            {
                if( historyPos == -1 )
                {
                    historyPos = history.Size - 1;
                }
                else if( historyPos > 0 )
                {
                    historyPos--;
                }
            }
            else if( data->EventKey == ImGuiKey_DownArrow )
            {
                if( historyPos != -1 )
                {
                    if( ++historyPos >= history.Size )
                    {
                        historyPos = -1;
                    }
                }
            }

            // A better implementation would preserve the data on the current input line along with cursor position.
            if( prev_history_pos != historyPos )
            {
                data->CursorPos = data->SelectionStart = data->SelectionEnd = data->BufTextLen = ( int )snprintf( data->Buf, ( size_t )data->BufSize, "%s", ( historyPos >= 0 ) ? history[historyPos] : "" );
                data->BufDirty = true;
            }
        }
    }

    return 0;
}


static int stricmpx( const char* str1, const char* str2 )
{ 
    int d; 
    while( ( d = toupper( *str2 ) - toupper( *str1 ) ) == 0 && *str1 )
    { 
        str1++; 
        str2++; 
    } 
    
    return d; 
}


static int strnicmpx( const char* str1, const char* str2, int n )
{ 
    int d = 0; 
    while( n > 0 && ( d = toupper( *str2 ) - toupper( *str1 ) ) == 0 && *str1 )
    { 
        str1++; 
        str2++; 
        n--; 
    } 
    
    return d; 
}


static char* strdupx( const char *str )
{ 
    size_t len = strlen( str ) + 1; 
    void* buff = malloc( len ); 
    
    return ( char* )memcpy( buff, ( const void* )str, len ); 
}

