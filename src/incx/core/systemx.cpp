/**
 * package: srsk
 * sub-package: src/incx/core/systemx.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// INCLUDES
#include "systemx.hpp"


// FORWARD DECLARATIONS
void framebuffer_size_callback( GLFWwindow* window, int width, int height );
void key_callback( GLFWwindow* window, int key, int scancode, int action, int mods );
void mouse_pos_callback( GLFWwindow* window, double xpos, double ypos );
void mouse_scroll_callback( GLFWwindow* window, double xoffset, double yoffset );
void mouse_button_callback( GLFWwindow* window, int button, int action, int mods );


// Globals
//orientationx* vcalc = nullptr;
camerax* camera = nullptr;
float lx, ly;                   // Last x and y coordinates of the mouse cursor
bool fm = true, am = false;     // First mouse move since right mouse button clicked flag, 
                                // and authorized motion flag triggered by right mouse button click
bool ui_component_flagged = false;
int  ui_component = 0;          // 0: none
                                // 1: Debug window
                                // 2: System configuration window


systemx::systemx( int screen_width, int screen_height )
{
    this->window = nullptr;
    this->graphics = nullptr;
    this->screenWidth = screen_width;
    this->screenHeight = screen_height;
    this->deltaTime = 0.0f;
    this->lastFrame = 0.0f;
    this->isJoyPresent = false;

    this->ui_show_loading_window = false;
}


systemx::~systemx()
{
}


int systemx::init()
{
    // Initialize GLFW
    if( !glfwInit() )
    {
        std::cout << "GLFW could not initialize" << std::endl;
        return -1;
    }
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 0 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    // Create the window object
    this->window = glfwCreateWindow( this->screenWidth, this->screenHeight, "The Sovereign Republic of Sihl Krel", NULL, NULL );
    if( this->window == NULL )
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent( this->window );
    
    // Register the resize framebuffer_size_callback function, mouse callback function, and mouse scroll callback function
    glfwSetFramebufferSizeCallback( this->window, framebuffer_size_callback );
    glfwSetKeyCallback( this->window, key_callback );
    glfwSetCursorPosCallback( this->window, mouse_pos_callback );
    glfwSetScrollCallback( this->window, mouse_scroll_callback );
    glfwSetMouseButtonCallback( this->window, mouse_button_callback );

    // Tell GLFW to capture our mouse
    glfwSetInputMode( this->window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );

    // Query input devices and initialize them
    this->queryInputDevices();

    // Initialize glad
    if( !gladLoadGLLoader( ( GLADloadproc )glfwGetProcAddress ) )
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        glfwTerminate();
        return -1;
    }

    // configure global opengl state
    glEnable( GL_DEPTH_TEST );

    // Setup ImGui binding
    ImGui_ImplGlfwGL3_Init( this->window, true );

    // Setup imgui default style
    ImGui::StyleColorsDark();   // Replace Dark with Classic for the original style

    // Load the desired UI font
    ImGuiIO& io = ImGui::GetIO();

    //io.Fonts->AddFontDefault();
    io.Fonts->AddFontFromFileTTF("incx/res/fonts/Roboto-Regular.ttf", 18.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("incx/res/fonts/Roboto-Bold.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    // At this point we've initialized the system enough to show a blank screen, and lead into
    // a loading screen for the end user to see - rather than a empty client that looks frozen
    // while the remainder of our resources load during initialization.
    this->ui_show_loading_window = true;
    this->showLoadingScreen( &this->ui_show_loading_window );

    // and continue initialization.

    // Instantiate our graphics object
    this->graphics = new graphicsx();
    if( !this->graphics )
    {
        std::cout << "Failed to create the graphicsx object" << std::endl;
        glfwTerminate();
        return -1;
    }

    std::cout << "Graphics object instantiated, initializing..." << std::endl;

    // Initialize the applications graphics
    if( !this->graphics->init( this->window, this->screenWidth, this->screenHeight ) )
    {
        std::cout << "Failed to initialize the application graphics" << std::endl;
        glfwTerminate();
        return -1;
    }


    // Instantiate the camera object
    camera = new camerax( this->graphics->playerModel, glm::vec3( 0.0f, 0.0f, 3.0f ) );
    if( !camera )
    {
        std::cout << "Failed to create the camerax object" << std::endl;
        glfwTerminate();
        return -1;
    }


    /* Instantiate our orientation calculator
    vcalc = new orientationx( glm::vec3( 0.0f, 0.0f, 3.0f ) );
    if( !vcalc )
    {
        std::cout << "Failed to create the orientationx object" << std::endl;
        glfwTerminate();
        return -1;
    }
    */

    // Initialize some globals
    lx = this->screenWidth / 2.0f;
    ly = this->screenHeight / 2.0f;

    return 0;
}


void systemx::run()
{
    while( !glfwWindowShouldClose( this->window ) )
    {
        // Per-frame time logic
        float currentFrame = glfwGetTime();
        this->deltaTime = currentFrame - lastFrame;
        this->lastFrame = currentFrame;

        // Input
        this->processInput( this->window );
        this->processUIInput();

        // Render logic here
        int loop = this->graphics->render( camera );

        // Check and call events, and swap the buffers
        glfwSwapBuffers( this->window );
        glfwPollEvents();
    }
}


void systemx::shutdown()
{
    // Destroy our camera object
    if( camera )
    {
        delete camera;
        camera = nullptr;
    }

    // Destroy our graphics object
    if( this->graphics )
    {
        this->graphics->shutdown();
        delete this->graphics;
        this->graphics = nullptr;
    }

    // Destroy our window object
    if( this->window )
    {
        //delete this->window;
        glfwTerminate();
        this->window = nullptr;
    }

}


void systemx::queryInputDevices()
{
    std::cout << "Checking for Joystick presence" << std::endl;
    for( int i = 0; i <= 16; i++ )
    {
        int joyPresent = glfwJoystickPresent( i );
        if( joyPresent )
        {
            // Get the JoyStick name and print information to the user
            const char* joyName = glfwGetJoystickName( i );
            std::cout << "GLFW_JOYSTICK_" << i << " present!" << std::endl;
            std::cout << "    Name: " << joyName << std::endl; 
            this->isJoyPresent = true;
            this->joyIsPresent.push_back( i );
        }
        else
        {
            //std::cout << "GLFW_JOYSTICK_" << i << " not present!" << std::endl;
        }
    }
}


void systemx::processInput( GLFWwindow* window )
{
    // The escape key should never be barred from working to exit the application.
    if( glfwGetKey( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose( window, true );
    }

    // This checks input states per frame, its not the place to configure
    // input in relation to UI elements, as it will not be smooth.  Use this
    // method to configure camera / object manipulation ONLY.
    ImGuiIO& io = ImGui::GetIO();
    if( !io.WantCaptureKeyboard )
    {
        if( glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS )
        {
            graphics->playerModel->processKeyboard( FORWARD, this->deltaTime );
            camera->processMovement();
        }

        if( glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS )
        {
            graphics->playerModel->processKeyboard( BACKWARD, this->deltaTime );
            camera->processMovement();
            //camera->processKeyboard( graphics->playerModel, BACKWARD, this->deltaTime );
        }

        if( glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS )
        {
            graphics->playerModel->processKeyboard( LEFT, this->deltaTime );
            camera->processMovement();
            //camera->processKeyboard( graphics->playerModel, LEFT, this->deltaTime );
        }

        if( glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS )
        {
            graphics->playerModel->processKeyboard( RIGHT, this->deltaTime );
            camera->processMovement();
            //camera->processKeyboard( graphics->playerModel, RIGHT, this->deltaTime );
        }
    }

    /**
     * 
     *  GLFW_JOYSTICK_0 BUTTON_STATE_0 triangle [right top]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_1 circle [right right]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_2 cross [right bottom]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_3 square [right left]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_4 L1 [rearLeft top]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_5 R1 [rearRight top]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_6 L2 [rearLeft Bottom]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_7 R2 [rearRight Bottom]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_8 Select [mid left]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_9 Start [mid right]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_10 L3 [LeftAnalog Button]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_11 R3 [RightAnalog Button]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_12 Up [LeftDirPad Up]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_13 Right [LeftDirPad Right]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_14 Down [LeftDirPad Down]
     *  GLFW_JOYSTICK_0 BUTTON_STATE_15 Left [LeftDirPad Left]
     * 
     * 
     */
    if( this->isJoyPresent )
    {
        for( int i = 0; i < this->joyIsPresent.size(); i++ )
        {
            int count = 0;
            const unsigned char* joyButtons = glfwGetJoystickButtons( this->joyIsPresent[i], &count );

            for( int j = 0; j < count; j++  )
            {
                if( joyButtons[j] == GLFW_PRESS )
                {
                    //std::cout << "GLFW_JOYSTICK_" << i << " BUTTON_" << j << " : Pressed" << std::endl;
                    if( j == 12 ) // Up directional pad
                    {
                        //camera->processKeyboard( FORWARD, this->deltaTime );
                        graphics->playerModel->processKeyboard( FORWARD, this->deltaTime );
                    }
                    if( j == 14 ) // Down directional pad
                    {
                        //camera->processKeyboard( BACKWARD, this->deltaTime );
                        graphics->playerModel->processKeyboard( BACKWARD, this->deltaTime );
                    }
                    if( j == 13 ) // Right directional pad
                    {
                        //camera->processKeyboard( RIGHT, this->deltaTime );
                        graphics->playerModel->processKeyboard( RIGHT, this->deltaTime );
                    }
                    if( j == 15 ) // Left directional pad
                    {
                        //camera->processKeyboard( LEFT, this->deltaTime );
                        graphics->playerModel->processKeyboard( LEFT, this->deltaTime );
                    }
                }
                if( joyButtons[j] == GLFW_RELEASE )
                {
                    //std::cout << "GLFW_JOYSTICK_" << i << " BUTTON_" << j << " : Released" << std::endl;
                }
            }

            /**
             * GLFW_JOYSTICK_0 JOYSTICK_AXIS_0 [Left X Axis] -1 [L] +1 [R]
             * GLFW_JOYSTICK_0 JOYSTICK_AXIS_1 [Left Y Axis] -1 [U] +1 [D]
             * GLFW_JOYSTICK_0 JOYSTICK_AXIS_2 [Right X Axis] -1 [L] +1 [R]
             * GLFW_JOYSTICK_0 JOYSTICK_AXIS_3 [Right Y Axis] -1 [U] +1 [D]
             */
            int axes = 0;
            const float* joyAxes = glfwGetJoystickAxes( i, &axes );
            for( int j = 0; j < axes; j++  )
            {
                if( joyAxes[j] != 0 )
                {
                    // Left Analog Stick for movement
                    if( j == 0 )    // X Axis
                    {
                        if( joyAxes[j] < -0.6f )
                        {   // Go Left
                            //camera->processKeyboard( LEFT, this->deltaTime );
                            graphics->playerModel->processKeyboard( LEFT, this->deltaTime );
                        }
                        if( joyAxes[j] > 0.6f )
                        {   // Go Right
                            //camera->processKeyboard( RIGHT, this->deltaTime );
                            graphics->playerModel->processKeyboard( RIGHT, this->deltaTime );
                        }
                    }
                    if( j == 1 )    // Y Axis
                    {
                        if( joyAxes[j] < -0.6f )
                        {   // Go Forward
                            //camera->processKeyboard( FORWARD, this->deltaTime );
                            graphics->playerModel->processKeyboard( FORWARD, this->deltaTime );
                        }
                        if( joyAxes[j] > 0.6f )
                        {   // Go Backward
                            //camera->processKeyboard( BACKWARD, this->deltaTime );
                            graphics->playerModel->processKeyboard( BACKWARD, this->deltaTime );
                        }
                    }

                    //std::cout << "GLFW_JOYSTICK_" << i << " JOYSTICK_AXIS_" << j << " : " << joyAxes[j] << std::endl;
                }
            }
        }
    }
}


void systemx::processUIInput()
{
    // Check for bound key input, and flag any corresponding ui components
    // So that our graphics & ui facilities render them accordingly
    if( ui_component_flagged )
    {
        switch( ui_component )
        {
            case 1:
            {
                this->graphics->ui_show_system_debug_window ^= 1;
            }break;

            case 2:
            {
                this->graphics->ui_show_system_configuration_window ^= 1;
            }break;
        }

        ui_component_flagged = false;
        ui_component = 0;
    }
}


void systemx::showLoadingScreen( bool* p_open )
{
    // First let us import a texture into openGL and get a texture id


    // Window color
    glViewport( 0, 0, this->screenWidth, this->screenHeight );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    // Call NewFrame first
    ImGui_ImplGlfwGL3_NewFrame();

    static bool no_titlebar = true;
    static bool no_scrollbar = true;
    static bool no_menu = true;
    static bool no_move = true;
    static bool no_resize = true;
    static bool no_collapse = true;
    static bool no_close = true;

    // Demonstrate the various window flags. Typically you would just use the default.
    ImGuiWindowFlags window_flags = 0;
    if( no_titlebar )  window_flags |= ImGuiWindowFlags_NoTitleBar;
    if( no_scrollbar ) window_flags |= ImGuiWindowFlags_NoScrollbar;
    if( !no_menu )     window_flags |= ImGuiWindowFlags_MenuBar;
    if( no_move )      window_flags |= ImGuiWindowFlags_NoMove;
    if( no_resize )    window_flags |= ImGuiWindowFlags_NoResize;
    if( no_collapse )  window_flags |= ImGuiWindowFlags_NoCollapse;
    if( no_close )     p_open = NULL; // Don't pass our bool* to Begin
    ImGui::SetNextWindowSize( ImVec2( 200, 50 ), ImGuiCond_FirstUseEver );
    ImGui::SetNextWindowPos( ImVec2( ( float )( this->screenWidth / 2 ), ( float )( this->screenHeight / 2 ) ) );
    ImGui::PushStyleColor( ImGuiCol_WindowBg, ImVec4( 0.0f, 0.0f, 0.0f, 0.0f ) );
    if( !ImGui::Begin( "Loading", p_open, window_flags ) )
    {
        // Early out if the window is collapsed, as an optimization.
        ImGui::End();
        return;
    }

    ImGui::Text( "Loading..." );

    ImGui::End();
    ImGui::PopStyleColor();

    ImGui::Render();

    glfwSwapBuffers( this->window );
    glfwPollEvents();
}


void framebuffer_size_callback( GLFWwindow* window, int width, int height )
{
    glViewport( 0, 0, width, height );
}


void key_callback( GLFWwindow* window, int key, int scancode, int action, int mods )
{
    ImGuiIO& io = ImGui::GetIO();
    if( action == GLFW_PRESS )
    {
        io.KeysDown[key] = true;
    }

    if( action == GLFW_RELEASE )
    {
        io.KeysDown[key] = false;
    }

    ( void )mods; // Modifiers are not reliable across systems
    io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
    io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
    io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
    io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];

    // bound keys that correspond to UI elements (shortcuts), should be configured here
    // in order to provide the proper experience to the end user.
    if( key == GLFW_KEY_F12 && action == GLFW_RELEASE )
    {
        ui_component_flagged = true;
        ui_component = 2;
    }

    if(  key == GLFW_KEY_F10 && action == GLFW_RELEASE )
    {
        ui_component_flagged = true;
        ui_component = 1;
    }

    if( key == GLFW_KEY_F7 && action == GLFW_RELEASE )
    {
        // draw in wireframe
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
}


void mouse_pos_callback( GLFWwindow* window, double xpos, double ypos )
{
    if( am )
    {
        // If its the first mouse movement
        if( fm )
        {
            lx = xpos;
            ly = ypos;
            fm = false;
        }

        // Calculate the x and y offsets
        float xo = xpos - lx;
        float yo = ly - ypos;  // Reversed since y-coordinates go from bottom to top

        lx = xpos;
        ly = ypos;

        //std::cout << "Processing mouse movement" << std::endl;
        camera->processMouseMovement( xo, yo );
    
    }
}


void mouse_scroll_callback( GLFWwindow* window, double xoffset, double yoffset )
{
    ImGuiIO& io = ImGui::GetIO();
    if( !io.WantCaptureMouse )
    {
        camera->processMouseScroll( yoffset );
    }

    ImGui_ImplGlfwGL3_ScrollCallback( window, xoffset, yoffset );
}


void mouse_button_callback( GLFWwindow* window, int button, int action, int mods )
{
    ImGuiIO& io = ImGui::GetIO();
    if( !io.WantCaptureMouse )
    {
        if( button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS )
        {
            //std::cout << "Right mouse button pressed" << std::endl;
            am = true;
        }

        if( button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE )
        {
            //std::cout << "Right mouse button released" << std::endl;
            am = false;
            fm = true;
        }

        if( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS )
        {
            //std::cout << "Left mouse button pressed" << std::endl;
        }

        if( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE )
        {
            //std::cout << "Left mouse button released" << std::endl;
        }
    }

    // Call the respective callback for the ui
    if( action == GLFW_PRESS && button >= 0 && button < 3 )
    {
        ImGui_ImplGlfwGL3_MouseButtonCallback( window, button, action, mods );
        //g_MouseJustPressed[button] = true;
    }
}