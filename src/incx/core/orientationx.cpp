/**
 * package: srsk
 * sub-package: src/incx/core/orientationx.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


#include "orientationx.hpp"
 

#define ORIENTATION_DEFAULT_FORWARD		( glm::vec3( 0, 0, - 1) )
#define ORIENTATION_DEFAULT_UP			( glm::vec3( 0, 1, 0 ) )
#define ORIENTATION_DEFAULT_RIGHT		( glm::vec3( 1, 0, 0 ) )
 
 
orientationx::orientationx( modelx* target, const glm::vec3& position ) : m_target( target ), m_position( position ), m_rotation( 1, 0, 0, 0 )
{}
 
 
orientationx::~orientationx()
{}
 
 
glm::vec3 orientationx::getPosition() const
{
	return m_position;
}
 
 
glm::vec3 orientationx::getForward() const
{
	return glm::rotate( m_rotation, ORIENTATION_DEFAULT_FORWARD );
}
 
 
glm::vec3 orientation::getUp() const
{
	return glm::rotate( m_rotation, ORIENTATION_DEFAULT_UP );
}
 
 
glm::vec3 orientationx::getRight() const
{
	return glm::rotate( m_rotation, ORIENTATION_DEFAULT_RIGHT );
}
 
 
glm::quat orientationx::getRotation() const
{
	return m_rotation;
}
 
 
void orientationx::setPosition( const glm::vec3& position )
{
	m_position = position;
}
 
 
void orientationx::move( const glm::vec3& vector )
{
	m_position += vector;
}
 
 
void orientationx::rotate( float angle, const glm::vec3& axis )
{
	m_rotation = glm::rotate( m_rotation, angle, axis );
}
 
 
void orientationx::roll( float angle )
{
	rotate( angle, ORIENTATION_DEFAULT_FORWARD );
}
 
 
void orientationx::yaw( float angle )
{
	rotate( angle, ORIENTATION_DEFAULT_UP );
}
 
 
void orientationx::pitch( float angle )
{
	rotate( angle, ORIENTATION_DEFAULT_RIGHT );
}
 
 
glm::mat4 orientationx::modelMatrix( const glm::mat4& scale ) const
{
	return glm::translate( m_position) * glm::toMat4( m_rotation ) * scale;
}
 
 
glm::mat4 orientationx::view() const
{
	return glm::lookAt( m_position, m_position + this->getForward(), this->getUp() );
}