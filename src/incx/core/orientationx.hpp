/**
 * package: srsk
 * sub-package: src/incx/core/orientationx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef ORIENTATIONX_HPP
#define ORIENTATIONX_HPP
 
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>

//#include "virtualx.hpp"
#include "modelx.hpp"
 
 
class orientationx
{
    public:
    
        orientationx( modelx* pmodel = nullptr, const glm::vec3& position = glm::vec3( 0, 0, 0 ) );
        virtual ~orientationx();
    
        glm::vec3 getPosition() const;
        glm::vec3 getForward() const;
        glm::vec3 getUp() const;
        glm::vec3 getRight() const;
        glm::quat getRotation() const;
    
        void setPosition( const glm::vec3& position );
        void move( const glm::vec3& vector );
        void rotate( float angle, const glm::vec3& axis );
        void roll( float angle );
        void yaw( float angle );
        void pitch( float angle );
    
        glm::mat4 modelMatrix( const glm::mat4& scale = glm::mat4( 1 ) ) const;
        glm::mat4 view() const;
    
    protected:
    
        glm::vec3 m_position;
        glm::quat m_rotation;
        modelx* m_target;
 
};