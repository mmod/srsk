/**
 * package: srsk
 * sub-package: src/incx/core/systemx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef SYSTEMX_HPP
#define SYSTEMX_HPP

#define SRSK_VERSION "0.0.1"


// INCLUDES
#include "graphicsx.hpp"


class systemx
{
    public:

        GLFWwindow* window;
        graphicsx* graphics;
        int screenWidth;
        int screenHeight;
        float deltaTime;
        float lastFrame;
        bool isJoyPresent;
        std::vector<int> joyIsPresent;

        bool loaded;

        systemx( int, int );
        ~systemx();

        int init();
        void run();
        void shutdown();
        void queryInputDevices();
        void processInput( GLFWwindow* );
        void processUIInput();
        void showLoadingScreen( bool* );

        bool ui_show_loading_window;
};


#endif