/**
 * package: srsk
 * sub-package: src/incx/core/modelx.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// INCLUDES
#include "modelx.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>


// FORWARD DECLARATIONS
unsigned int textureFromFile( const char* path, const std::string &directory, bool gamma = false );


modelx::modelx( std::string const &path, bool gamma, glm::vec3 positionx, glm::vec3 upx, float yawx, float pitchx ) : gammaCorrection( gamma ), front( glm::vec3( 0.0f, 0.0f, -1.0f ) ), movementSpeed( SPEED ), mouseSensitivity( SENSITIVITY ), zoom( ZOOM )
{
    this->position = positionx;
    this->worldUp = upx;
    this->yaw = yawx;
    this->pitch = pitchx;

    this->updateObjectVectors();
    this->loadModel( path );
}


//modelx::modelx( std::string const &path, bool gamma /*= false*/, float x, float y, float z, float X, float Y, float Z, float yawx, float pitchx ) : gammaCorrection( gamma ), front( glm::vec3( 0.0f, 0.0f, -1.0f ) ), movementSpeed( SPEED ), mouseSensitivity( SENSITIVITY ), zoom( ZOOM )
//{
//    this->position = glm::vec3( x, y, z );
//    this->worldUp = glm::vec3( X, Y, Z );
//    this->yaw = yawx;
//    this->pitch = pitchx;

//    loadModel( path );
//}


void modelx::draw( shaderx* shader )
{
    for( unsigned int i = 0; i < this->meshes.size(); i++ )
    {
        this->meshes[i].draw( shader );
    }
}


/**
 * loadModel
 * 
 * loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
 */
void modelx::loadModel( std::string const &path )
{
    // Read file via ASSIMP
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile( path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace );

    // Check for errors
    if( !scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode ) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }

    //std::cout << "Just checked for errors..." << std::endl;

    // Retrieve directory path of the filepath
    this->directory = path.substr( 0, path.find_last_of( '/' ) );

    //std::cout << "Model directory: " << this->directory.c_str() << std::endl;

    // Process ASSIMP's root node recursively
    this->processNode( scene->mRootNode, scene );
}


/**
 * processNode
 * 
 * processes a node in a recursive fashion. Processes each individual mesh located at the node and 
 * repeats this process on its children nodes (if any).
 */
void modelx::processNode( aiNode* node, const aiScene* scene )
{

    //std::cout << "Processing node..." << std::endl;
    // Process each mesh located at the current node
    for( unsigned int i = 0; i < node->mNumMeshes; i++ )
    {
        // The node object only contains indices to index the actual objects in the scene
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes)
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        this->meshes.push_back( this->processMesh( mesh, scene ) );
    }

    // After weve processed all of the meshes (if any) we then recursively process each of the children nodes
    for( unsigned int i = 0; i < node->mNumChildren; i++ )
    {
        this->processNode( node->mChildren[i], scene );
    }
}


meshx modelx::processMesh( aiMesh* mesh, const aiScene* scene )
{

    //std::cout << "Processing data to fill..." << std::endl;
    std::vector<vertexx> vertices;
    std::vector<unsigned int> indices;
    std::vector<texturex> textures;

    //std::cout << "Walk through each of the mesh's vertices." << std::endl;
    for( unsigned int i = 0; i < mesh->mNumVertices; i++ )
    {
        //std::cout << "    #" << i << std::endl;
        vertexx vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        
        //std::cout << "        Positions" << std::endl;
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.position = vector;

        // Set position of model as the position of the first mesh of the first node, so rhe cam has a target
        if( i == 0 )
        {
            this->position = vertex.position;
        }
        
        //std::cout << "        Normals" << std::endl;
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.normal = vector;
        //std::cout << "        Texture Coords" << std::endl;
        if( mesh->mTextureCoords[0] ) // does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x; 
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.texCoords = vec;
        }
        else
        {
            vertex.texCoords = glm::vec2( 0.0f, 0.0f );
        }
        //std::cout << "        Tangent" << std::endl;
        vector.x = mesh->mTangents[i].x;
        vector.y = mesh->mTangents[i].y;
        vector.z = mesh->mTangents[i].z;
        vertex.tangent = vector;
        //std::cout << "        Bitangent" << std::endl;
        vector.x = mesh->mBitangents[i].x;
        vector.y = mesh->mBitangents[i].y;
        vector.z = mesh->mBitangents[i].z;
        vertex.bitangent = vector;
        vertices.push_back( vertex );
    }
    
    //std::cout << "Walk through each of the mesh's faces (a face is a mesh; its triangle)" << std::endl;
    //std::cout << "and retrieve the corresponding vertex indices." <<std::endl;
    for( unsigned int i = 0; i < mesh->mNumFaces; i++ )
    {
        //std::cout << "    #" << i << std::endl;
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for( unsigned int j = 0; j < face.mNumIndices; j++ )
        {
            indices.push_back( face.mIndices[j] );
        }
    }
    // process materials
    //std::cout << "Processing materials..." << std::endl;
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];    
    // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
    // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
    // Same applies to other texture as the following list summarizes:
    // diffuse: texture_diffuseN
    // specular: texture_specularN
    // normal: texture_normalN

    //std::cout << "    1 Diffuse maps" << std::endl;    // 1
    std::vector<texturex> diffuseMaps = this->loadMaterialTextures( material, aiTextureType_DIFFUSE, "texture_diffuse" );
    textures.insert( textures.end(), diffuseMaps.begin(), diffuseMaps.end() );
    
    //std::cout << "    2 Specular maps" << std::endl;    // 2
    std::vector<texturex> specularMaps = this->loadMaterialTextures( material, aiTextureType_SPECULAR, "texture_specular" );
    textures.insert( textures.end(), specularMaps.begin(), specularMaps.end() );
    
    //std::cout << "    3 Normal maps" << std::endl;    // 3
    std::vector<texturex> normalMaps = this->loadMaterialTextures( material, aiTextureType_HEIGHT, "texture_normal" );
    textures.insert( textures.end(), normalMaps.begin(), normalMaps.end() );
    
    //std::cout << "    4 Height maps" << std::endl;    // 4
    std::vector<texturex> heightMaps = this->loadMaterialTextures( material, aiTextureType_AMBIENT, "texture_height" );
    textures.insert( textures.end(), heightMaps.begin(), heightMaps.end() );
    
    //std::cout << "Returning a mesh object created from the extracted mesh data." << std::endl;    // 1
    return meshx( vertices, indices, textures );
}


/**
 * loadMaterialTextures
 * 
 * checks all material textures of a given type and loads the textures if they're not loaded yet.
 * the required info is returned as a Texture struct.
 */
std::vector<texturex> modelx::loadMaterialTextures( aiMaterial* mat, aiTextureType type, std::string typeName )
{
    std::vector<texturex> textures;
    for( unsigned int i = 0; i < mat->GetTextureCount( type ); i++ )
    {
        aiString str;
        mat->GetTexture( type, i, &str );
        // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
        bool skip = false;
        for( unsigned int j = 0; j < textures_loaded.size(); j++ )
        {
            if( std::strcmp( textures_loaded[j].path.data(), str.C_Str()) == 0 )
            {
                textures.push_back( textures_loaded[j] );
                skip = true; // a texture with the same filepath has already been loaded, continue to next one. (optimization)
                break;
            }
        }
        if( !skip )
        {   // if texture hasn't been loaded already, load it
            texturex texture;
            texture.id = textureFromFile( str.C_Str(), this->directory );
            texture.type = typeName;
            texture.path = str.C_Str();
            textures.push_back( texture );
            textures_loaded.push_back( texture );  // store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
        }
    }
    return textures;
}


unsigned int textureFromFile( const char* path, const std::string &directory, bool gamma )
{
    std::string filename = std::string( path );
    filename = directory + '/' + filename;

    unsigned int textureID;
    glGenTextures( 1, &textureID );

    int width, height, nrComponents;
    unsigned char* data = stbi_load( filename.c_str(), &width, &height, &nrComponents, 0 );
    if( data )
    {
        GLenum format;
        if( nrComponents == 1 )
        {
            format = GL_RED;
        }
        else if( nrComponents == 3 )
        {
            format = GL_RGB;
        }
        else if( nrComponents == 4 )
        {
            format = GL_RGBA;
        }

        glBindTexture( GL_TEXTURE_2D, textureID );
        glTexImage2D( GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data );
        glGenerateMipmap( GL_TEXTURE_2D );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

        stbi_image_free( data );
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free( data );
    }

    return textureID;
}


void modelx::processKeyboard( SPATIAL_MOVEMENT direction, float deltaTime )
{
    float velocity = this->movementSpeed * deltaTime;

    if( direction == FORWARD )
    {
        // We have to update all the vertices in every mesh
        for( int i = 0; i < this->meshes.size(); i++ )
        {   // For every mesh

            for( int j = 0; j < this->meshes[i].vertices.size(); j++ )
            {   // for every vertex
                this->meshes[i].vertices[j].position += this->front * velocity;
            }

            // And then update the buffer data that is to be drawn
            this->meshes[i].updateMesh();
        }

        // We have to update the model instances position, as the mesh positions arent taken 
        // into consideration for the camera's position.
        this->position += this->front * velocity;
    }
    
    if( direction == BACKWARD )
    {
        // We have to update all the vertices in every mesh
        for( int i = 0; i < this->meshes.size(); i++ )
        {   // For every mesh

            for( int j = 0; j < this->meshes[i].vertices.size(); j++ )
            {   // for every vertex
                this->meshes[i].vertices[j].position -= this->front * velocity;
            }

            // And then update the buffer data that is to be drawn
            this->meshes[i].updateMesh();
        }

        // We have to update the model instances position, as the mesh positions arent taken 
        // into consideration for the camera's position.
        this->position -= this->front * velocity;
    }
    
    if( direction == LEFT )
    {
        // We have to update all the vertices in every mesh
        for( int i = 0; i < this->meshes.size(); i++ )
        {   // For every mesh

            for( int j = 0; j < this->meshes[i].vertices.size(); j++ )
            {   // for every vertex
                this->meshes[i].vertices[j].position -= this->right * velocity;
            }

            // And then update the buffer data that is to be drawn
            this->meshes[i].updateMesh();
        }

        // We have to update the model instances position, as the mesh positions arent taken 
        // into consideration for the camera's position.
        this->position -= this->right * velocity;
    }
    
    if( direction == RIGHT )
    {
        // We have to update all the vertices in every mesh
        for( int i = 0; i < this->meshes.size(); i++ )
        {   // For every mesh

            for( int j = 0; j < this->meshes[i].vertices.size(); j++ )
            {   // for every vertex
                this->meshes[i].vertices[j].position += this->right * velocity;
            }

            // And then update the buffer data that is to be drawn
            this->meshes[i].updateMesh();
        }

        // We have to update the model instances position, as the mesh positions arent taken 
        // into consideration for the camera's position.
        this->position += this->right * velocity;
    }
}


void modelx::updateObjectVectors()
{
    // Calculate new front vectors
    glm::vec3 frontx;
    frontx.x = cos( glm::radians( this->yaw ) ) * cos( glm::radians( this->pitch ) );
    frontx.y = sin( glm::radians( this->pitch ) );
    frontx.z = sin( glm::radians( this->yaw ) ) * cos( glm::radians( this->pitch ) );
    this->front = glm::normalize( frontx );

    // Also re-calculate the right and up vectors
    this->right = glm::normalize( glm::cross( this->front, this->worldUp ) );
    this->up = glm::normalize( glm::cross( this->right, this->front ) );
}