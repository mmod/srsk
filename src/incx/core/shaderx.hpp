/**
 * package: srsk
 * sub-package: src/incx/core/shaderx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef SHADERX_HPP
#define SHADERX_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>


class shaderx
{
    public:

        unsigned int id;

        shaderx( const char*, const char*, const char* );
        ~shaderx();

        void use();
        void setBool( const std::string &, bool );
        void setInt( const std::string &, int );
        void setFloat( const std::string &, float );
        void setVec2( const std::string &, const glm::vec2 & );
        void setVec2( const std::string &, float, float );
        void setVec3( const std::string &, const glm::vec3 & );
        void setVec3( const std::string &, float, float, float );
        void setVec4( const std::string &, const glm::vec4 & );
        void setVec4( const std::string &, float, float, float, float );
        void setMat2( const std::string &, const glm::mat2 & );
        void setMat3( const std::string &, const glm::mat3 & );
        void setMat4( const std::string &, const glm::mat4 & );

    private:

        void checkCompileErrors( GLuint, std::string );

};


#endif