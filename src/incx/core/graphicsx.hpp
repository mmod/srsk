/**
 * package: srsk
 * sub-package: src/incx/core/graphicsx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef GRAPHICSX_HPP
#define GRAPHICSX_HPP

#define SRSK_VERSION "0.0.1"

// INCLUDES
#include <glad/glad.h>
//#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <GLFW/glfw3.h>


#include "shaderx.hpp"
#include "camerax.hpp"
//#include "orientationx.hpp"
//#include "virtualx.hpp"
#include "modelx.hpp"

#include <imgui/imgui.h>
#include "imgui_impl_glfw_gl3.h"



#include <iostream>


class graphicsx
{
    public:

        int screenWidth;
        int screenHeight;

        unsigned int vbo;
        unsigned int vao;
        unsigned int lvao;
        unsigned int texture1;
        unsigned int texture2;
        int width;
        int height;
        int nrChannels;
        unsigned char* data;

        glm::vec3 lightPos;

        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 model;

        shaderx* shader;
        shaderx* lampShader;
        modelx* playerModel;

        //CEGUI::OpenGLRenderer& ceguiRenderer;
        //CEGUI::DefaultResourceProvider* rp;
        //CEGUI::XMLParser* ceguiXMLParser;
        //CEGUI::WindowManager& ceguiWinMgr;
        //CEGUI::Window* ceguiRootWin;
        //CEGUI::FrameWindow* ceguiFWin;


        graphicsx();
        ~graphicsx();

        int init( GLFWwindow*, int, int );
        int render( camerax* );
        int renderUI();
        void showSystemDebugWindow( bool* );
        void showSystemConfigurationWindow( bool* );
        void showSystemLogWindow( bool* );
        void shutdown();


        // UI flags & settings keys
        bool ui_show_system_log_window;
        //bool ui_system_log_scroll_to_bottom;
        bool ui_show_system_debug_window;
        bool ui_show_system_metrics_window;
        bool ui_show_system_configuration_window;
        bool ui_show_style_editor_window;
        bool ui_show_system_console_window;
        bool ui_show_demo_window;

        ImVec4 dev_tool_color_picker_value;

};


#endif