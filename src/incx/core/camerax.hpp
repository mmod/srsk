/**
 * package: srsk
 * sub-package: src/incx/core/camerax.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef CAMERAX_HPP
#define CAMERAX_HPP


// INCLUDES
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//#include "virtualx.hpp"
#include "modelx.hpp"

#include <vector>


class camerax
{
    public:

        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;
        glm::vec3 right;
        glm::vec3 worldUp;

        float yaw;
        float pitch;

        float movementSpeed;
        float mouseSensitivity;
        float zoom;

        modelx* targetx;

        camerax( modelx* pmodel = nullptr, glm::vec3 positionx = glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3 upx = glm::vec3( 0.0f, 1.0f, 0.0f ), float yawx = YAW, float pitchx = PITCH ) : front( glm::vec3( 0.0f, 0.0f, -1.0f ) ), movementSpeed( SPEED ), mouseSensitivity( SENSITIVITY ), zoom( ZOOM )
        {
            this->position = positionx;
            this->worldUp = upx;
            this->yaw = yawx;
            this->pitch = pitchx;

            this->targetx = pmodel;

            this->updateCameraVectors();
        }
        camerax( modelx* pmodel, float x, float y, float z, float X, float Y, float Z, float yawx, float pitchx ) : front( glm::vec3( 0.0f, 0.0f, -1.0f ) ), movementSpeed( SPEED ), mouseSensitivity( SENSITIVITY ),  zoom( ZOOM )
        {
            this->position = glm::vec3( x, y, z );
            this->worldUp = glm::vec3( X, Y, Z );
            this->yaw = yawx;
            this->pitch = pitchx;

            this->targetx = pmodel;

            this->updateCameraVectors();
        }

        ~camerax()
        {
        }

        glm::mat4 getViewMatrix()
        {
            //std::cout << "Player model position: [x] " << this->model->position.x << "  [y] " << this->model->position.y << "  [z] " << this->model->position.z << std::endl;
            return glm::lookAt( this->position, this->front, this->up );
        }

        void processMovement()
        {
            //float velocity = this->movementSpeed * deltaTime;
                
            //this->updateCameraVectors();
            glm::vec4 cp;
            //cp.x = 10.0f * cos( glm::radians( this->pitch ) ) * sin( glm::radians( this->yaw ) );
            //cp.z = 10.0f * cos( glm::radians( this->pitch ) ) * cos( glm::radians( this->yaw ) );
            cp.x = 10.0f * sin( glm::radians( this->yaw ) );
            cp.z = 10.0f * cos( glm::radians( this->yaw ) );
            this->position = glm::vec3( this->targetx->position.x + cp.x, this->targetx->position.y, this->targetx->position.z + cp.z + 10.0f );


            //this->position = glm::vec3( this->targetx->position.x, this->targetx->position.y, this->targetx->position.z + 10.0f );
            //this->position = glm::vec3( this->targetx->position.x, this->targetx->position.y, this->targetx->position.z + 10.0f );
            //std::cout << "Cam pos [" << this->position.x << ", " << this->position.y << ", " << this->position.z << "]  Target pos [" << this->targetx->position.x << ", " << this->targetx->position.y << ", " << this->targetx->position.z << "]" << std::endl;
            
            //if( direction == FORWARD )
            //{
                //this->position += this->front * velocity;
            //}
            
            //if( direction == BACKWARD )
            //{
                //this->position -= this->front * velocity;
            //}
            
            //if( direction == LEFT )
            //{
                //this->position -= this->right * velocity;
            //}
            
            //if( direction == RIGHT )
            //{
                //this->position += this->right * velocity;
            //}

            
        }

        void processMouseMovement( float xoffset, float yoffset, GLboolean constrainPitch = true )
        {
            xoffset *= this->mouseSensitivity;
            yoffset *= this->mouseSensitivity;

            this->yaw += xoffset;
            this->pitch += yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't get flipped
            if( constrainPitch )
            {
                if( this->pitch > 89.0f )
                {
                    this->pitch = 89.0f;
                }
                if( this->pitch < -89.0f )
                {
                    this->pitch = -89.0f;
                }
            }

            // Update front, right, and up vectors using the updated eular angles
            this->updateCameraVectors();
        }

        void processMouseScroll( float yoffset )
        {
            if( this->zoom >= 1.0f && this->zoom <= 45.0f )
            {
                this->zoom -= yoffset;
            }
            if( this->zoom <= 1.0f )
            {
                this->zoom = 1.0f;
            }
            if( this->zoom >= 45.0f )
            {
                this->zoom = 45.0f;
            }
        }

    private:

        void updateCameraVectors()
        {
            //this->right = glm::normalize( glm::cross( this->targetx->position, this->worldUp ) );
            //this->up = glm::normalize( glm::cross( this->targetx->position, this->right ) );
            glm::vec3 targetVec;
            glm::vec3 rightx;
            glm::vec3 upx;
            
            targetVec = glm::normalize( this->position - this->targetx->position );
            rightx = glm::normalize( glm::cross( targetVec, this->worldUp ) );
            upx = glm::normalize( glm::cross( rightx, targetVec ) );

            this->front = targetVec;
            this->right = rightx;
            this->up = upx;
        }

};


#endif