/**
 * package: srsk
 * sub-package: src/incx/core/meshx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef MESHX_HPP
#define MESHX_HPP


// INCLUDES
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shaderx.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

struct vertexx {
    // position
    glm::vec3 position;
    // normal
    glm::vec3 normal;
    // texCoords
    glm::vec2 texCoords;
    // tangent
    glm::vec3 tangent;
    // bitangent
    glm::vec3 bitangent;
};

struct texturex {
    unsigned int id;
    std::string type;
    std::string path;
};

class meshx
{
    public:

        std::vector<vertexx> vertices;
        std::vector<unsigned int> indices;
        std::vector<texturex> textures;
        unsigned int vao;

        meshx( std::vector<vertexx> vertices, std::vector<unsigned int> indices, std::vector<texturex> textures )
        {
            this->vertices = vertices;
            this->indices = indices;
            this->textures = textures;

            // Now that we have all the required data, set the vertex buffers and its attribute pointers
            setupMesh();
        }

        // render the mesh
        void draw( shaderx* shader ) 
        {
            // bind appropriate textures
            unsigned int diffuseNr  = 1;
            unsigned int specularNr = 1;
            unsigned int normalNr   = 1;
            unsigned int heightNr   = 1;

            for( unsigned int i = 0; i < this->textures.size(); i++)
            {
                glActiveTexture( GL_TEXTURE0 + i ); // active proper texture unit before binding
                // retrieve texture number (the N in diffuse_textureN)
                std::string number;
                std::string name = this->textures[i].type;
                if( name == "texture_diffuse" )
                {
                    number = std::to_string( diffuseNr++ );
                }
                else if( name == "texture_specular" )
                {
                    number = std::to_string( specularNr++ ); // transfer unsigned int to stream
                }
                else if( name == "texture_normal" )
                {
                    number = std::to_string( normalNr++ ); // transfer unsigned int to stream
                }
                else if( name == "texture_height" )
                {
                    number = std::to_string( heightNr++ ); // transfer unsigned int to stream
                }

                // now set the sampler to the correct texture unit
                glUniform1i( glGetUniformLocation( shader->id, ( name + number ).c_str() ), i );
                // and finally bind the texture
                glBindTexture( GL_TEXTURE_2D, this->textures[i].id );
            }
            
            // draw mesh
            glBindVertexArray( this->vao );
            glDrawElements( GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0 );
            glBindVertexArray( 0 );

            // always good practice to set everything back to defaults once configured.
            glActiveTexture( GL_TEXTURE0 );
        }

        void updateMesh()
        {
            glBindVertexArray( this->vao );
            // load data into vertex buffers
            glBindBuffer( GL_ARRAY_BUFFER, this->vbo );
            // A great thing about structs is that their memory layout is sequential for all its items.
            // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
            // again translates to 3/2 floats which translates to a byte array.
            glBufferData( GL_ARRAY_BUFFER, this->vertices.size() * sizeof( vertexx ), &this->vertices[0], GL_STATIC_DRAW );  

            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->ebo );
            glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof( unsigned int ), &this->indices[0], GL_STATIC_DRAW );
            glBindVertexArray( 0 );
        }

    private:

        unsigned int vbo;
        unsigned int ebo;

        void setupMesh()
        {
            // create buffers/arrays
            glGenVertexArrays( 1, &this->vao );
            glGenBuffers( 1, &this->vbo );
            glGenBuffers( 1, &this->ebo );

            glBindVertexArray( this->vao );
            // load data into vertex buffers
            glBindBuffer( GL_ARRAY_BUFFER, this->vbo );
            // A great thing about structs is that their memory layout is sequential for all its items.
            // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
            // again translates to 3/2 floats which translates to a byte array.
            glBufferData( GL_ARRAY_BUFFER, this->vertices.size() * sizeof( vertexx ), &this->vertices[0], GL_STATIC_DRAW );  

            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->ebo );
            glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof( unsigned int ), &this->indices[0], GL_STATIC_DRAW );

            // set the vertex attribute pointers
            // vertex Positions
            glEnableVertexAttribArray( 0 );	
            glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( vertexx ), ( void* )0 );
            // vertex normals
            glEnableVertexAttribArray( 1 );	
            glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( vertexx ), ( void* )offsetof( vertexx, normal ) );
            // vertex texture coords
            glEnableVertexAttribArray( 2 );	
            glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( vertexx ), ( void* )offsetof( vertexx, texCoords ) );
            // vertex tangent
            glEnableVertexAttribArray( 3 );
            glVertexAttribPointer( 3, 3, GL_FLOAT, GL_FALSE, sizeof( vertexx ), ( void* )offsetof( vertexx, tangent ) );
            // vertex bitangent
            glEnableVertexAttribArray( 4 );
            glVertexAttribPointer( 4, 3, GL_FLOAT, GL_FALSE, sizeof( vertexx ), ( void* )offsetof( vertexx, bitangent ) );

            glBindVertexArray( 0 );
        }


        
};


#endif