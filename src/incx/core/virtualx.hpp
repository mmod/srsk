/**
 * package: srsk
 * sub-package: src/incx/core/virtualx.hpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */
#ifndef VIRTUALX_HPP
#define VIRTUALX_HPP


// INCLUDES
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>


// Defines several possible options for movement.  Used as abstraction to stay away from window-system specific input methods
enum SPATIAL_MOVEMENT {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

// Default Camera values
const float YAW             = -90.0f;
const float PITCH           =   0.0f;
const float SPEED           =   2.5f;
const float SENSITIVITY     =   0.1f;
const float ZOOM            =  45.0f;


class virtualx      // As it exists within our virtual realm
{
    public:

        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;
        glm::vec3 right;
        glm::vec3 worldUp;

        float yaw;
        float pitch;

        float movementSpeed;
        float mouseSensitivity;
        float zoom;

        virtualx();
        ~virtualx();
        virtual void processKeyboard( SPATIAL_MOVEMENT direction, float deltaTime ) const = 0;

};


#endif