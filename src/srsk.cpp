/**
 * package: srsk
 * sub-package: src/srsk.cpp
 * author: Richard B. Winters <a href="mailto:devrikx@gmail.com">devrikx AT gmail DOT com</a>
 * copyright: 2011-2018 Richard B WInters.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


#include <iostream>
#include <core/systemx.hpp>


systemx* sysx =  nullptr;


int main( int argc, char* argv[] )
{
    std::cout << "Hello" << std::endl;
    std::cout << "Creating systemx object" << std::endl;
    sysx = new systemx( 1280, 720 );

    if( sysx == nullptr )
    {
        std::cout << "Failed to create systemx object" << std::endl;
        return -1;
    }

    std::cout << "Initializing OpenGL Engine" << std::endl;
    int didNotInitialize = 1;
    didNotInitialize = sysx->init();
    
    if( didNotInitialize )
    {
        std::cout << "Failed to initialize OpenGL" << std::endl;
        return -1;
    }
    
    std::cout << "Entering system loop" << std::endl;
    sysx->run();

    std::cout << "System shutting down" << std::endl;
    sysx->shutdown();
    delete sysx;
    sysx = nullptr;

    return 0;
}