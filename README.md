# SRSK

An original title.  The project itself is a beginning to an OpenGL Game Engine.

While the file organization and break-out into separate classes exists - this project is **STRONGLY** based upon the tutorials, up-to and including the lighting section, found on LearnOpenGL.com. 

Additions & Changes diverging from the tutorial include:

 - The modelx & meshx classes include properties and methods for tracking and setting position for movement within the application.
 - The camera is being modified to function as a 3rd-person camera that follows our nanosuit model (not currently functioning)
 - A loading screen is implemented to smooth entrance into the application while resources - and OpenGL - intialize.
 - A simple GUI is implemented using IMGUI
 - For now, lighting is disabled in the shader, though all code for phong-based lighting is present.
 - The mouse is not used to look around, and instead will function to rotate around its target when the right-mouse button is held-down.


<br />
## Dependencies

The includes for all dependencies are included in the repository. Binary files are also included for Windows systems, no building required to test this application.  If you'd like to build this application for Linux, you'll need to build the following **SHARED** dependencies:

 - GLFW
 - ASSIMP


 <br />
## Building & Running

To build the application, on Windows
 - MSys2 is the build system used. 
 - Inspect the .vscode directory, specifically the tasks.json file, to see the build command and configuration. 
 - Assuming you use the Visual Studio Code editor and the included configuration, you can simply run the finished executable once the build completes.

For Linux 
 - Inspect the .vscode directory, specifically the tasks.json file, to see the build command and configuration. 
 - You'll likely need to place shared libraries in the same directory as the executable to then run the application. 
 - Ensure the finished executable ends up in the root of this project, so that required dependencies - such as fonts, textures, and model objects - can be found by the executable at run-time.
 - A makefile based on the Debian environment will be included in future commits.


## Contributing

For now, the 3rd-person camera is the next change incoming. If you'd like to help, feel free to submit a pull request.  The following would be expected:

 - Changes to the camera will be implemented within the existing camerax class as much as possible.
 - The changes will follow the organization, and standards, present in the current project. This includes syntax formatting, which is **FIRM** for any of our projects.


<br />
